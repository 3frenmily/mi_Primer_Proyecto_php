<?php
//Regresar un valor en una funcion
function resta($num){
  return $num-10;
}
echo resta(40);

function hacer_cafe($tipo="capuchino"){
  return " El cafe es de $tipo \n";
}

echo hacer_cafe();
echo hacer_cafe(null);
echo hacer_cafe("moka");

function persona($nombre="fulanito",$apellido="fulanito",$edad=15){
  return "  Bienvenido $nombre $apellido de $edad años \n";
}
echo persona("Efren","Arvea");

function animales($animales=array("sin dato","sin dato"),$tipo="Sin tipo"){
  return "Los animales son ".implode(",",$animales)." $tipo";

}
echo animales();
echo animales(array("Gato","Tigre"),"felinos");

//Recibir multiples videos
function flores(){
  $flor="Clavel";
  $color="Amarillo";
  $cantidad=20;
  return array($flor,$color,$cantidad);
}
list($fl,$cl,$cant)=flores();
echo "Hola soy $fl mi color es $cl somos $cant";
 ?>
