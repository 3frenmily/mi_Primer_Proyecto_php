<?php
//Funciones anonimas
$saludo=function($nombre){
  echo $nombre;
};
$saludo('Efren');
$suma=function($num1,$num2){
  return $num1+$num2;
};
echo "--";
echo $suma(2,5);

//Funciones variables
function saludo($saludo){
  echo "--Soy la variable funcion $saludo";
}
$variable = "saludo";
echo $variable("Hola");

//Funciones internas de php
if(function_exists("xml_parser_create")):
  echo "La funcion existe";
else:
  echo "La funcion no existe";
endif;

print_r(get_extension_funcs("xml"));
 ?>
