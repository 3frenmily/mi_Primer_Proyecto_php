<?php
//Argumentos de funciones
function suma($num1,$num2){
  $suma = $num1+$num2;
  echo "La suma es $suma";
}
//suma(10,15);
//array
suma_array(array(10,5,4,6));
function suma_array($entrada){
  $num1=$entrada[0];
  $num2=$entrada[1];
  echo "El resultado es ".($num1+$num2);
}

//funcion por referencia
function resta(&$num){
  $num=20-$num;
}
$result=13;
resta($result);

//funcion recursiva
function recursiva($a){
  if($a<20){
    echo "$a\n";
    recursiva($a+1);
  }
}
recursiva(1);
 ?>
